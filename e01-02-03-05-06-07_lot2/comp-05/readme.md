<<<<<<< HEAD
# Évaluation compétence 5 lot 2

Il vous est demander de développer la base de données d'une application en vous référant au compétences 1, 2 et 3 de l'évaluation (entrants et corrections).  

L'application à développer est un suivi de popularité de blagues sur Chuck Norris.

## Objectifs

- Savoir créer une base de données
- Savoir créer un seeding de la base

## Réalisation

- Durée : **Mercredi 21 août 2019 9h00 - mercredi 21 août 2019 17h00**. Tout retard ne sera pas corrigé.
- Groupe : **En solo**

## Contraintes

- Pas d'éléments externes
- Framework autorisés

## Rendu

- Un modèle Merise ;
- Un script SQL contenant la base. Le script devra :
    - **être écrit manuellement** (et non exporté d'un application),
    - **prendre en compte les problèmatiques de sécurité** ;
- Un script SQL contenant un seed. Il devra contenir 13 membres du jury, toutes les catégories ainsi qu'une dizaine de blagues.

## Critères de validation

### Compétence 5 : Créer une base de données

- La base de données est conforme au schéma physique.
- Le script de création de bases de données s’exécute sans erreurs.
- Le script d’insertion des données de test s’exécute sans erreurs.
- La base de données est disponible avec les droits d'accès prévus.
- La base de données de test peut être restaurée en cas d'incident.
- Les besoins de sécurité du SGBD sont exprimés selon l’état de l’art et les exigences de sécurité identifiées.
- L’objet de la recherche est exprimé de manière précise en langue française ou anglaise.
- La documentation technique liée aux technologies associées, en français ou en anglais, est comprise (sans contre-sens, ...).
- La démarche de recherche permet de résoudre un problème technique ou de mettre en oeuvre une nouvelle fonctionnalité.
- Le partage du résultat de recherche et de veille est effectué, oralement ou par écrit, avec ses pairs.
||||||| merged common ancestors
=======
# Évaluation compétence 5 lot 2

Il vous est demander de développer la base de données d'une application en vous référant au compétences 1, 2 et 3 de l'évaluation (entrants et corrections).  

L'application à développer est un suivi de popularité de blagues sur Chuck Norris.

## Objectifs

- Savoir créer une base de données
- Savoir créer un seeding de la base

## Réalisation

- Durée : **Mercredi 21 août 2019 9h00 - mercredi 21 août 2019 17h00**. Tout retard ne sera pas corrigé.
- Groupe : **En solo**

## Rendu

- Un schéma de la base de données (modèle Merise ou autre) ;
- Un script SQL contenant la base. Le script devra :
    - **être écrit manuellement** (et non exporté d'un application),
    - **prendre en compte les problèmatiques de sécurité** ;
- Un script SQL contenant un seed. Il devra contenir 13 membres du jury, toutes les catégories ainsi qu'une dizaine de blagues.

## Critères de validation

### Compétence 5 : Créer une base de données

- La base de données est conforme au schéma physique.
- Le script de création de bases de données s’exécute sans erreurs.
- Le script d’insertion des données de test s’exécute sans erreurs.
- La base de données est disponible avec les droits d'accès prévus.
- La base de données de test peut être restaurée en cas d'incident.
- Les besoins de sécurité du SGBD sont exprimés selon l’état de l’art et les exigences de sécurité identifiées.
- L’objet de la recherche est exprimé de manière précise en langue française ou anglaise.
- La documentation technique liée aux technologies associées, en français ou en anglais, est comprise (sans contre-sens, ...).
- La démarche de recherche permet de résoudre un problème technique ou de mettre en oeuvre une nouvelle fonctionnalité.
- Le partage du résultat de recherche et de veille est effectué, oralement ou par écrit, avec ses pairs.
>>>>>>> 62d64045b930fedebf3bf2288bab39b318ef54ed
