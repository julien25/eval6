<?php
/**
 * Helper claas for translating class names into table names
 *
 * PHP version 7.2
 *
 * @author   Hans Vanpee <hvanpee.ext@simplon.co>
 * @file
 * @license  Hans Vanpee, 2019, https://simplon.co
 * @link     https://simplon.co
 */

namespace Simplon\Jokes\Helper;

class Language
{
    /* English singular -> plural exceptions */
    private const EXCEPTIONS = [
        'solo' => 'solos',
        'piano' => 'pianos',
        'piccolo' => 'piccolos',
        'cello' => 'cellos',
        'wife' => 'wives',
        'shelf' => 'shelves',
        'knife' => 'knives',
        'elf' => 'elves',
    ];
    private const VOWELS = ['a', 'e', 'i', 'o', 'u'];

    /**
     * Remove namespace path from classname and convert to lowercase
     *
     * @param $fqcn string Fully Qualified Class name
     *
     * @returns string The class name
     */
    private static function stripClassPath($fqcn)
    {
        $className = $fqcn;
        $lastSeparatorPos = strrpos($className, '\\');
        if ($lastSeparatorPos !== false) {
            $className = substr($className, $lastSeparatorPos + 1);
        }
        return $className;
    }

    public static function convertToTableName(string $className)
    {
        $tableName = self::stripClassPath($className);
        // Check for camelcase, insert underscore where needed
        $tableName = strtolower(preg_replace('/([a-z])([A-Z])/', '${1}_${2}', $tableName));

        $tableNameLength = strlen($tableName);
        $lastChar = $secondLastChar = '';
        if ($tableNameLength > 0) {
            $lastChar = $tableName[$tableNameLength - 1 ];
        }
        if ($tableNameLength > 1) {
            $secondLastChar = $tableName[$tableNameLength - 2];
        }

        // Convert singular to plural (less or more)
        if (array_key_exists($tableName, self::EXCEPTIONS)) {
            $tableName = self::EXCEPTIONS['$tableName'];
        } elseif (in_array($lastChar, ['y', 'o']) && in_array($secondLastChar, self::VOWELS)) {
            $tableName .= 's';
        } elseif ($secondLastChar == 'i' && $lastChar == 's') {
            $tableName = substr_replace($tableName, 'e', $tableNameLength - 2, 1);
        } elseif ($lastChar == 'y') {
            $tableName = substr_replace($tableName, 'ies', $tableNameLength -1, 1);
        } elseif (in_array($lastChar, ['s', 'z', 'x'])
            || ($lastChar == 'h' && in_array($secondLastChar, ['c', 's']))) {
            $tableName .= 'es';
        }
        // Now try to convert to plural for English words
        // Words ending in:
        // -y -> -ies
        // -e -> -es
        $nameLength = strlen($tableName);
        switch ($tableName[$nameLength - 1]) {
            case 'ay':
            case 'ey':
            case 'iy':
            case 'oy':
            case 'uy':
                $tableName .= 's';
                break;
            case 'y':
                $tableName = substr($tableName, 0, $nameLength - 1) . 'ies';
                break;
            case 'o':
            case 's':
            case 'z':
            case 'x':
            case 'ch':
            case 'sh':
                $tableName .= 'es';
                break;
            case 'e':
            case 'a':
            default:
                $tableName .= 's';
                break;
        }

        return $tableName;
    }
}
