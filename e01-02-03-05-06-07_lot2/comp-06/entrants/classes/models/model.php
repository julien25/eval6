<?php
/**
 * Model base class
 *
 * PHP version 7.2
 *
 * @author   Hans Vanpee <hvanpee.ext@simplon.co>
 * @file
 * @license  Hans Vanpee, 2019, https://simplon.co
 * @link     https://simplon.co
 */

namespace Simplon\Jokes\Models;

use Simplon\Jokes\Config\Config;
use Simplon\Jokes\Helper\Language;

/**
 * The abstract model class
 *
 * Each derived class has the name of the database table. The classname is
 * converted to lowercase before matching the tablename.
 *
 * @category ProjectSimplon
 * @package  BattleGame
 * @author   Hans Vanpee <hvanpee.ext@simplon.co>
 * @license  Hans Vanpee, 2019, https://simplon.co
 * @link     https://simplon.co
 */
abstract class Model
{
    /* Keep one db object for all Model objects */
    private static $db = null;

    /* Buffer tablenames for improved performance */
    private static $tableNames = [];

    /* Store field values, used by getter and setter */
    private $fields = [];

    /* Flag to indicate new record */
    private $isNew = true;

    /* Keep track of relationships */
    private static $hasManyList = null;

    /**
     * Create new object from fields list
     *
     * @param array $fields An associative array with field values
     *
     * @return void
     */
    public function __construct(array $fields, bool $isNew = true)
    {
        $this->fields = $fields;
        $this->isNew = $isNew;

        /* Check if class is initialized */
        if (self::$hasManyList === null) {
            self::$hasManyList = [];
            static::setRelations();
        }
    }

    /**
     * Get a connection to the database
     *
     * @return boolean true if connection OK, false when failed
     */
    private static function getDB()
    {
        // Get parameters from config file
        if (!self::$db) {
            $dbConf = Config::getArray('database');
            if (!$dbConf) {
                throw new \Exception('Could not get database config.');
            }

            // Check if all keys are present
            if (!array_key_exists('server', $dbConf)
                || !array_key_exists('user', $dbConf)
                || !array_key_exists('database', $dbConf)
                || !array_key_exists('password', $dbConf)
            ) {
                throw new \Exception('Missing database config key');
            }

            // Construct DSN string
            $dsn = "mysql:host={$dbConf['server']}";
            $dsn .= ";dbname={$dbConf['database']}";
            $dsn .= ";charset=utf8mb4";

            // Set connection options: throw exceptions when errors occur
            // and use FETCH_ASSOC by default.
            $options = [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            ];
            self::$db = new \PDO($dsn, $dbConf['user'], $dbConf['password'], $options);
        }

        return self::$db;
    }
    /**
     * Find tablename using classname
     *
     * @return string The name of the database table
     */
    private static function getTableName()
    {
        if (!in_array(static::class, self::$tableNames)) {
            self::$tableNames[static::class] = Language::convertToTableName(static::class);
        }
        return self::$tableNames[static::class];
    }

    /**
     * Execute a select * statement on the table and return the resultset.
     *
     * Takes an optional array with select, where and order instructions
     * Example:
     * [
     * 'SELECT' => ['col1', 'col2', 'col3'],
     * 'WHERE' => ['col1' => 'val1', 'col2' => 'val2']]
     * 'ORDER' => ['col1' => 'A', 'col2' => 'D']
     * ]
     *
     * @param array $options An array with WHERE and SELECT options
     *
     * @return array The resultset array containing derived class objects.
     */
    public static function getAll(array $options = null) : array
    {
        $tableName = self::getTableName();
        $columns = '*';
        $where = '';
        if ($options != null) {
            if (array_key_exists('SELECT', $options)) {
                // Sanitize column names and create select string
                $columns = implode(', ', array_map(function ($el) {
                    return "`$el`";
                }, $options['SELECT']));
            }
            if (array_key_exists('WHERE', $options)) {
                $where = " WHERE " . implode(' AND ', array_map(function ($el) {
                    return "`{$el}` = ?";
                }, array_keys($options['WHERE'])));
            }
        }
        $queryString = "SELECT {$columns} FROM {$tableName}{$where};";
        $stmt = self::getDB()->prepare($queryString);
        if (strlen($where)) {
            $stmt->execute(array_values($options['WHERE']));
        } else {
            $stmt->execute();
        }
        $result = $stmt->fetchAll();
        $objectArray = [];
        $class = static::class;
        foreach ($result as $row) {
            $objectArray[] = new $class($row, false);
        }
        return $objectArray;
    }

    /**
     * Get value for fieldname
     *
     * @return mixed Field value
     */
    public function __get($fieldName)
    {
        if (array_key_exists($fieldName, $this->fields)) {
            return $this->fields[$fieldName];
        }
        throw new Exception("Field not found: {$fieldName}");
    }

    /**
     * Set value for fieldname
     *
     * @param string $fieldName The name of the database column
     * @param mixed $fieldValue The value for the column
     *
     * @return void
     */
    public function __set($fieldName, $fieldValue)
    {
        $this->fields[$fieldName] = $fieldValue;
    }

    /**
     * If id is null, store new record, otherwise update existing record
     */
    public function store()
    {
        if ($this->isNew) {
            $queryString = 'INSERT INTO '
                . self::getTableName()
                . '('
                . implode(', ', array_keys($this->fields))
                . ') values('
                . implode(', ', array_map(function ($el) {
                    return ':' . $el;
                }, array_keys($this->fields)))
                . ')';
        } else {
            $queryString = 'UPDATE '
                . self::getTableName()
                . ' SET '
                . implode(
                    ', ',
                    array_map(
                        function ($val, $key) {
                            return "$key=:$key";
                        },
                        array_filter($this->fields, function ($fld) {
                            return $fld !== 'id';
                        }),
                        array_keys($this->fields)
                    )
                );
            $queryString .= " WHERE id=:id";
        }
        $stmt = self::getDB()->prepare($queryString);
        $stmt->execute($this->fields);
        if ($this->isNew) {
            if (!isset($this->fields['id'])) {
                $this->fields['id'] = self::getDB()->lastInsertId();
            }
            $this->isNew = false;
        }
    }

    /**
     * Find record for given id
     *
     * @return class Model the record found or null
     */
    public static function find($id)
    {
        $stmt = self::getDB()->prepare("SELECT * FROM `" . self::getTableName() . "` WHERE ID = :id");
        $stmt->execute(['id' => $id]);
        $result = $stmt->fetchAll();
        if (count($result) > 0) {
            $className = static::class;
            return new $className($result[0], false);
        } else {
            return null;
        }
    }

    /**
     * Delete the current model
     */
    public function delete()
    {
        /* Delete related models if needed */
        if (count(self::$hasManyList) > 0) {
            foreach (self::$hasManyList as $hasMany) {
                $class = $hasMany['targetModel'];
                $class::deleteAll([$hasMany['targetColumn'] => $this->id]);
            }
        }
        $queryString = "DELETE FROM " . $this->getTableName() . " WHERE id=:id";
        $stmt = $this->getDB()->prepare($queryString);
        $stmt->execute(['id' => $this->fields['id']]);
    }

    /**
     * Delete all records from table
     *
     * @param $options Optional array with query options
     *
     * Options syntax:
     * ['where' => ['field1' => value1], 'field2' => value2, ...]
     */
    public static function deleteAll(array $where)
    {
        $queryString = "DELETE FROM " . self::getTableName();
        if ($where !== null) {
            $queryString .=
                ' WHERE ' .
                implode(
                    ' AND ',
                    array_map(
                        function ($val, $key) {
                            return "$key = :$key";
                        },
                        $where,
                        array_keys($where)
                    )
                );
        }
        $stmt = self::getDB()->prepare($queryString);
        if ($where !== null) {
            $stmt->execute($where);
        } else {
            $stmt->execute();
        }
    }

    /**
     * Define a ralationship with another model
     *
     * The relationship is based on the local column named "id"
     *
     * @param $targetModel the name of the target model
     * @param $targetColumn the column used for the relationship
     *
     */
    protected static function hasMany($targetModel, $targetColumn)
    {
        self::$hasManyList[] = ['targetModel' => $targetModel, 'targetColumn' => $targetColumn];
    }
}
