<?php
/**
 * The config class
 *
 * This class will handle configuration value retrieval
 *
 * PHP version 7.2
 *
 * @author   Hans Vanpee <hvanpee.ext@simplon.co>
 * @file
 * @license  Hans Vanpee, 2019, https://simplon.co
 * @link     https://simplon.co
 */

namespace Simplon\Jokes\Config;

/**
 * The config class
 *
 * @author   Hans Vanpee <hvanpee.ext@simplon.co>
 * @license  Hans Vanpee, 2019, https://simplon.co
 * @link     https://simplon.co
 */
class Config
{
    private const CONFIG_DIR = 'config';
    private static $documentRoot = null;

    /**
     * Get the current document root (web or cli)
     *
     * @return string The document root directory or null
     */
    private static function getDocumentRoot()
    {
        if (self::$documentRoot == null) {
            self::$documentRoot = $_SERVER['DOCUMENT_ROOT'];
            if (!self::$documentRoot) {
                // Running in cli? Try to get pwd
                self::$documentRoot = getenv('PWD');
            }
        }
        return self::$documentRoot;
    }
    /**
     * Read config file value
     *
     * @param string $configFile The name of a config file
     * @param string $key        The name of a config key
     *
     * @return mixed The value or null if not found
     */
    public static function getValue($configFile, $key)
    {
        $fullPath = self::getDocumentRoot() . DIRECTORY_SEPARATOR;
        $fullPath .= self::CONFIG_DIR . DIRECTORY_SEPARATOR . $configFile . '.php';
        if (file_exists($fullPath)
            && ($configArray = include $fullPath)
            && array_key_exists($key, $configArray)
        ) {
            return $configArray[$key];
        }
        return null;
    }

    /**
     * Read config file array
     *
     * @param string $configFile The name of a config file
     *
     * @return mixed The array or null if not found
     */
    public static function getArray($configFile)
    {
        $fullPath = self::getDocumentRoot() . DIRECTORY_SEPARATOR;
        $fullPath .= self::CONFIG_DIR . DIRECTORY_SEPARATOR . $configFile . '.php';
        if (file_exists($fullPath)) {
            return include $fullPath;
        }
        return null;
    }
}
