<?php
/**
 * The router for the application
 *
 * PHP version 7.2
 *
 * @author   Hans Vanpee <hvanpee.ext@simplon.co>
 * @file
 * @license  Hans Vanpee, 2019, https://simplon.co
 * @link     https://simplon.co
 */

namespace Simplon\Jokes\Router;

use Simplon\BattleGame\Controller\MainController;

/**
 * The router class reads the routeslist and selects a route depending on the
 * actual URI
 *
 * @category ProjectSimplon
 * @package  BattleGame
 * @author   Hans Vanpee <hvanpee.ext@simplon.co>
 * @license  Hans Vanpee, 2018, https://simplon.co
 * @link     https://simplon.co
 */
class Router
{
    /**
     * Call the action for the current URI
     *
     * @param array $routesList An associative array with paths as keys and
     * actions as values. When a route ends in /{id} the last segment is a
     * placeholder for an unsigned integer ID.
     *
     * @return void
     */
    public static function route(array $routesList)
    {
        // Get HTTP method, check for tunneled method
        if (array_key_exists('_method', $_POST)) {
            $method = $_POST['_method'];
        } elseif (array_key_exists('_method', $_GET)) {
            $method = $_GET['_method'];
        } else {
            $method = $_SERVER['REQUEST_METHOD'];
        }

        // Get URI, replace IDs with {id} and store values in idList
        $parsedUri = parse_url($_SERVER['REQUEST_URI']);
        $routeSegments = explode('/', $parsedUri['path']);
        $idList = [];
        for ($i = 0; $i < count($routeSegments); $i++) {
            if (preg_match('/^[0-9]+$/', $routeSegments[$i])) {
                $idList[] = $routeSegments[$i];
                $routeSegments[$i] = '{id}';
            }
        }
        $baseUri = implode("/", $routeSegments);

        if (array_key_exists($baseUri, $routesList)
            // An existing route was requested
            && array_key_exists($method, $routesList[$baseUri])) {
            $routesList[$baseUri][$method](...$idList);
        } else {
            $extension = pathinfo($parsedUri['path'], PATHINFO_EXTENSION);
            $documentRoot = $_SERVER['DOCUMENT_ROOT'];
            $filePath = $documentRoot . DIRECTORY_SEPARATOR . $parsedUri['path'];

            // Check if a file exists for the request
            if (!file_exists($filePath)) {
                header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not found', true, 404);
                die();
            }
            switch ($extension) {
                case 'css':
                    header('Content-Type: text/css');
                    break;
                case 'js':
                    header('Content-Type: application/javascript');
                    break;
            }
            $content = file_get_contents($filePath);
            echo $content;
        }
    }
}
