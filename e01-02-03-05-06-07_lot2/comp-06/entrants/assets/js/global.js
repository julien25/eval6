(()=>{
  let getParentByTagName = (node, tagname) => {
    let parent
    if (node === null || tagname === '') return
    
    parent = node.parentNode
    tagname = tagname.toUpperCase()
  
    while (parent.tagName !== "HTML") {
      if (parent.tagName === tagname) return parent
      parent = parent.parentNode
    }
  
    return parent
  }


  /* Update */
  // Show inputs
  document.querySelectorAll('.joke-action-std .btn').forEach(btn => {
    btn.addEventListener('click', e => {
      let tr = getParentByTagName(btn, 'tr')
      
      tr.querySelector('.joke-action-std').setAttribute('aria-hidden', 'true')
      tr.querySelector('.joke-action-edit').setAttribute('aria-hidden', 'false')
      tr.querySelectorAll('td:not(:last-child)').forEach(td => {
        td.innerHTML = `<input type="text" name="${td.getAttribute('headers').split('-')[1]}" value="${td.innerHTML}" class="form-field">`
      })
    })
  })
 
  // Hide inputs
  document.querySelectorAll('.joke-action-edit .btn-error').forEach(btn => {
    btn.addEventListener('click', e => {
      let tr = getParentByTagName(btn, 'tr')
      
      tr.querySelector('.joke-action-std').setAttribute('aria-hidden', 'false')
      tr.querySelector('.joke-action-edit').setAttribute('aria-hidden', 'true')
      tr.querySelectorAll('td:not(:last-child)').forEach(td => td.innerHTML = td.querySelector('input').value)
    })
  })
 
  // Go form
  document.querySelectorAll('.joke-action-edit .btn-success').forEach(btn => {
    btn.addEventListener('click', e => {
      let f = getParentByTagName(btn, 'form')

      f.querySelector('[name="id-joke"]').value = getParentByTagName(btn, 'tr').id.split('-')[1]
      f.querySelector('[name="action"]').value = 'update'
    })
  })


  /* Delete */
  // Show delete element
  document.querySelectorAll('.joke-action-std .btn-error').forEach(btn => {
    btn.addEventListener('click', e => getParentByTagName(btn, 'tr').querySelector('.joke-action-del').setAttribute('aria-hidden', 'false'))
  })

  // Hide delete element
  document.querySelectorAll('.joke-action-del .btn-snd').forEach(btn => {
    btn.addEventListener('click', e => getParentByTagName(btn, 'tr').querySelector('.joke-action-del').setAttribute('aria-hidden', 'true'))
  })

  // Go form
  document.querySelectorAll('.joke-action-del .btn').forEach(btn => {
    btn.addEventListener('click', e => {
      let f = getParentByTagName(btn, 'form')

      f.querySelector('[name="id-joke"]').value = getParentByTagName(btn, 'tr').id.split('-')[1]
      f.querySelector('[name="action"]').value = 'delete'
    })
  })
})()