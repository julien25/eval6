<?php
/**
 * AutoLoader for jokes application
 *
 * All classes are stored under the "classes" directories and subdirectories.
 * Directory and filenames should be all lowercase.
 * For the project the namespace prefix is Simplon\Jokes.
 *
 * PHP version 7.2
 *
 * @author   Hans Vanpee <hvanpee.ext@simplon.co>
 * @file
 * @license  Hans Vanpee, 2019, https://simplon.co
 * @link     https://simplon.co
 */

namespace Simplon\Jokes\AutoLoader;

const BASE_DIR = '/classes';
const NAMESPACE_PREFIX = 'Simplon\Jokes';
const APP_CLASS = '\App\\';

spl_autoload_register(function (string $className) {
    // Convert FQCN to filepath:
    // Utility classes are searched in the classes subdirectory
    // App classes are found in the app subdirecotory
    // Examples:
    // - Simplon\Jokes\Database\Model -> document_root/classes/database/model.php
    // - Simplon\Jokes\App\Models\Role -> document_root/app/models/role.php

    // First check if FQCN starts with Simplon\BattleGame
    if (strpos($className, NAMESPACE_PREFIX) === 0) {
        // get stripped classname (remove prefix)
        // This looks like \Database\Table or \App\Models\Role
        $strippedClassName = substr($className, strlen(NAMESPACE_PREFIX));

        // Decide which subdir to use
        $subdirPrefix = strpos($strippedClassName, APP_CLASS) === false ? BASE_DIR : '';

        // get absolute base path
        $pathName = $_SERVER['DOCUMENT_ROOT'];
        if (!$pathName) {
            $pathName = getenv('PWD');
        }

        // Concatenate path segments
        $pathName .= $subdirPrefix
            . strtolower($strippedClassName)
            . '.php';

        // Replace namespace separators by directory separators and convert all to lowercase
        $pathName = str_replace('\\', DIRECTORY_SEPARATOR, $pathName);

        // Check if target exists
        if (!file_exists($pathName)) {
            throw new \Exception("File for class $pathName not found");
        }

        // Target found, include file
        include $pathName;
    } else {
        // Not found: the class name must use the prefix
        throw new \Exception("Can't find class: $className");
    }
});
