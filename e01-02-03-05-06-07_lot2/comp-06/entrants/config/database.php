<?php
/**
 * Database connection parameters
 *
 * PHP version 7.2
 *
 * @author   Hans Vanpee <hvanpee.ext@simplon.co>
 * @file
 * @license  Hans Vanpee, 2019, https://simplon.co
 * @link     https://simplon.co
 */
return [
    'server' => 'localhost',
    'user' => 'joker',
    'database' => 'jokes',
    'password' => 'badjoke'
];
