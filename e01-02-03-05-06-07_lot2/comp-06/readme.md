# Évaluation compétence 6 lot 2


D'après les entrants et corrections des compétences 1, 2, 3 et 5 de l'évaluation et de la maquette admin-jokes.php (route de l'application : '/admin-jokes'), développer des modèles d'accès aux données.  

L'application à développer est un suivi de popularité de blagues sur Chuck Norris.

## Objectifs

- Savoir créer des modèles d'accès aux données.

## Réalisation

- Durée : **Jeudi 22 août 2019 9h00 - Jeudi 22 août 2019 17h00**. Tout retard ne sera pas corrigé.
- Groupe : **En solo**

## Contraintes

- Respecter les standards.
- **Documenter son code** d'après les standards.
- Frameworks autorisés

## Rendu

- Le code source de l'application.

## Spécifications fonctionnelles 

Le code devra respecter les contraintes suivantes :  

- Le choix du langage backend et/ou framework est libre.
- Les règles de sécurité sont respectées.

Le modèle devra permettre de :

- sélectioner toutes les blagues,
- ajouter une nouvelle blague,
- mettre à jour une blague (changer la date, la catégorie, etc),
- supprimer une blague ainsi que tous les votes qui lui sont rattachés.
Au moins 2 de ces 4 fonctionnalités doivent être implémentées.

### Mise en place

1. Prendre le code situé dans le dossier "entrants" (vous n'avez pas besoin de modifier le front)
2. Mettre en place la base de données en fonction de la correction de la compétence 5
3. Supprimer les lignes "statiques" des deux tableaux de la page, vous pouvez vous aider du template (lignes 34 à 63) pour vos résultats

### Code

Une fois la page vidée :

1. Sélectionner toutes les blagues de la base de données dans le premier tableau
2. Sélectionner toutes les blagues de la catégorie "explicit" dans le second tableau
3. Au clic sur la validation d'un bouton de suppression, supprimer la blague correspondante dans la base de données
4. Au clic sur la validation d'un bouton de modification, mettre à jour la blague correspondante dans la base de données
5. Au clic sur l'enregistrement d'une nouvelle blague, rajouter la blague dans la base de données

En sachant que chaque envoie de formulaire (points 3, 4 et 5) se fait en POST avec les variables suivantes :

- $_POST['id-joke'] : contient l'id de la blague à modifier. Est vide dans le cas d'un ajout de blague.
- $_POST['action'] : vaut 'update' (modification de blague, point 3), 'delete' (suppression de blague, point 4) ou 'add' (ajout de blague, point 5). C'est lui qui détermine quel type d'action est demandée.
- $_POST['name'] : contient la blague. Optionnel.
- $_POST['cat'] : contient la catégorie de la blague. Optionnel. Peut être vide.
- $_POST['date'] : contient la date de création de la blague. Optionnel. Si vide et si action == 'add', alors il faut rajouter la date en cours.

## Critères de validation

### Compétence 6 : Développer les composants d'accès aux données.

- Les traitements relatifs aux manipulations des données répondent aux fonctionnalités décrites dans le dossier de conception technique ;
- Un test unitaire est associé à chaque composant, avec une double approche fonctionnelle et sécurité ;
- Les composants d’accès à la base de données suivent les règles de sécurisation reconnues ;
- La sécurité des composants d’accès se fonde sur les mécanismes de sécurité du SGBD ;
- L’objet de la recherche est exprimé de manière précise en langue française ou anglaise ;
- La documentation technique liée aux technologies associées, en français ou en anglais, est comprise (sans contre-sens, ...) ;
- La démarche de recherche permet de résoudre un problème technique ou de mettre en oeuvre une nouvelle fonctionnalité ;
- Le partage du résultat de recherche et de veille est effectué, oralement ou par écrit, avec ses pairs ;
- Le code source est documenté ou auto-documenté ;
- La veille sur les vulnérabilités connues permet d’identifier et corriger des failles potentielles.
