<?php

namespace App\Http\Controllers;

use App\Jury;
use Illuminate\Http\Request;

class JuryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('jurys.index',compact('jurys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jurys.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'avatar' => 'required',
        ]);

        Product::create($request->all());
   
        return redirect()->route('jurys.index')
                        ->with('success','Jury created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jury  $jury
     * @return \Illuminate\Http\Response
     */
    public function show(Jury $jury)
    {
        return view('jurys.show',compact('jury'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jury  $jury
     * @return \Illuminate\Http\Response
     */
    public function edit(Jury $jury)
    {
        return view('jurys.edit',compact('jury'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jury  $jury
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jury $jury)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'avatar' => 'required',
        ]);

        $product->update($request->all());
  
        return redirect()->route('jurys.index')
                        ->with('success','Jury updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jury  $jury
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jury $jury)
    {
        $product->delete();
  
        return redirect()->route('juryss.index')
                        ->with('success','Jury deleted successfully');
    }
}
