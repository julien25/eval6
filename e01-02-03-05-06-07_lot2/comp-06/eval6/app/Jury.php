<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jury extends Model
{
    protected $fillable = [
        'firstname', 'lastname', 'avatar'
    ];
}