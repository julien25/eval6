<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = [
        'jury_id', 'joke_id', 'lol', 'bof'
    ];
}