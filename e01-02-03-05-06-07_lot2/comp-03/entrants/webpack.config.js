// npm i -D autoprefixer babel-core babel-loader babel-preset-env cross-env css-hot-loader css-loader expose-loader extract-text-webpack-plugin file-loader iconfont-plugin-webpack node-sass optimize-css-assets-webpack-plugin postcss-loader resolve-url-loader sass-loader style-loader uglifyjs-webpack-plugin webpack webpack-cli webpack-concat-plugin webpack-dashboard webpack-dev-server webpack-livereload-plugin webpack-merge-and-include-globally

const webpack = require("webpack");
const path = require("path");
const ExtractTextWebpackPlugin = require("extract-text-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssets = require("optimize-css-assets-webpack-plugin");
const DashboardPlugin = require("webpack-dashboard/plugin");
const ConcatPlugin = require('webpack-concat-plugin');
const IconfontPlugin = require('iconfont-plugin-webpack');
const LiveReloadPlugin = require('webpack-livereload-plugin');

let config = {
  entry: ["./assets/js/global.js", "./assets/scss/styles.scss"],
  output: {
    path: path.resolve(__dirname , "./public"),
    filename: "./js/bundle.js"
  },
  module: {
      rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },
      {
        test: /\.scss$/,
        use: ["css-hot-loader"].concat(ExtractTextWebpackPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader','sass-loader'],
        }))
      },
      {
        test: /\.(woff|woff2|eot|otf|ttf|svg|png|jpg)$/,
        loader: 'file-loader',
        options: {
          name: '../[folder]/[name].[ext]'
        }
      }]
    },
    plugins: [
      new ConcatPlugin({
        uglify: true,
        sourceMap: true,
        name: 'scripts',
        outputPath: "js",
        fileName: '[name].js',
        filesToConcat: ['./assets/js/**'],
        attributes: {
          async: true
        }
      }),
      new IconfontPlugin({
        src: path.resolve(__dirname , './assets/iconfont'),
        family: 'iconfont',
        dest: {
          font: path.resolve(__dirname ,'./assets/fonts/[family].[type]'),
          css: path.resolve(__dirname ,'./assets/scss/_[family].scss')
        },
        watch: {
          pattern: './assets/iconfont/*.svg',
          cwd: __dirname
        }
      }),
      new ExtractTextWebpackPlugin("css/styles.css"),
      new DashboardPlugin(),
      new LiveReloadPlugin()
    ],
    devServer: {
      contentBase: path.resolve(__dirname , "./public"),
      historyApiFallback: true,
      inline: true,
      open: true,
      hot: true
    },
    devtool: "eval-source-map",
    optimization: {
      minimizer: []
    }
  }
  module.exports = config;

if (process.env.NODE_ENV === "production") {
  module.exports.plugins.push(
    new OptimizeCSSAssets()
  );
  module.exports.optimization.minimizer.push(
    new UglifyJsPlugin()
  );
}
