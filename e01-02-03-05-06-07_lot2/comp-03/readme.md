# Évaluation compétence 3 lot 2
L'application à développer est un suivi de popularité de blagues sur Chuck Norris.

## Objectifs

- À partir d'une maquette HTML, développer un script client.

## Réalisation

- Durée : **Mardi 20 août 2019 9h00 - Mardi 20 août 2019 17h00**. Tout retard ne sera pas corrigé.
- Groupe : **En solo**

## Contraintes

- Frameworks et librairies acceptés.
- Pas de code récupéré.
- Respecter les standards.
- **Documenter son code** d'après les standards.

## Entrants

- Les sujets et correction des évaluations 1 et 2 
- Le code source de l'application dans le docssier corrections/comp-02
- La maquette jury.html

## Rendu

- Le code source de l'application.

## Réalisation

D'après les readme des compétences précédentes et des maquettes HTML de la compétence 2, intégrer le script client des pages 'Ajouter une blague' et 'Editer une blague' d'après le scénario utilisateur suivant. Nous considérons que l'utilisateur vient de cliquer sur le bouton "Ajouter une blague" de la page d'accueil et a été redirigé sur la page 'Ajouter une blague' (le développement commence ici).  
Le script doit faire les étapes ci-dessous. 
**Tous les cas** doivent être pris en compte (n'oubliez pas les erreurs et la sécurité).

### Étape 1 : ajout d'une blague

1. L'utilisateur clique sur le bouton "Chuck it !", une requête AJAX est envoyée sur l'[API Chuck Norris](https://api.chucknorris.io) pour récupérer une blague au hasard. Si la requête retourne :
    - une blague, le script peut continuer
    - tout autre chose, un message doit être affiché à la place de la blague en fonction du problème (résultat vide, erreur 500, etc)
2. Si tout s'est bien passé, la blague est ajouté dans le DOM tel que défini dans le wireframe :
    - La blague est affichés dans le titre
    - La date est affichée sous le titre au format JJ/MM/AAAA
    - **Niveau 2** La catégorie est affichée au dessus du titre seulement si elle existe

### Étape 2 : vote de la blague

C'est au tour du jury de voter. 
Un jury ne peut pas supprimer son vote (il peut néanmoins le changer).

1. L'utilisateur vote pour chaque juré.
2. Lors du clique sur le bouton "Voter", le pourcentage de votes positifs est compté ((nombre de votes positifs*100)/nombre de votes totaux)
3. Une fois le pourcentage obtenu, vous pouvez soit :
    - remplacer le texte 'Vote en cours' par 'Cette blague a été aimée par XX% des votants' où XX correspond au pourcentage de votes positifs
    - cacher le texte 'Vote en cours', afficher la progressbar avec le bon pourcentage de vote positif (la div .vote-like devra avoir un css inline tel que width:XX%) et, en fond de cette partie, un gif animé provenant de l'[API yesNo](https://yesno.wtf). Il sera :
        - yes, si le vote est supérieur à 50%,
        - no, si le vote est inférieur à 50%,
        - maybe, si le vote est égal à 50% 
    Une attention toute particulière sera apportée à ceux qui auront travailler sur le second cas.

## Critères de validation

### Compétence 3 : Développer une interface utilisateur web dynamique.

- Les pages web respectent la charte graphique de l'entreprise ;
- Les bonnes pratiques de structuration et de sécurité sont respectées y compris pour le web mobile ;
- L’objet de la recherche est exprimé de manière précise en langue française ou anglaise ;
- La documentation technique liée aux technologies associées, en français ou en anglais, est comprise (sans contre-sens, ...) ;
- La démarche de recherche permet de résoudre un problème technique ou de mettre en oeuvre une nouvelle fonctionnalité ;
- Le partage du résultat de recherche et de veille est effectué, oralement ou par écrit, avec ses pairs ;
- L'architecture de l'application répond aux bonnes pratiques de développement et de sécurisation d'application web ;
- L’application web est optimisée pour les équipements mobiles ;
- Le code source est documenté ou auto-documenté ;
- L'application web est publiée sur un serveur ;
- La veille sur les vulnérabilités connues permet d’identifier et corriger des failles potentielles.