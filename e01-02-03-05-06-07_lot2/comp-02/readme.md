# Évaluation compétence 2 lot 2
L'application à développer est un suivi de popularité de blagues sur Chuck Norris.

## Objectifs

- Savoir intégrer une maquette.
- Savoir prendre en compte les problématique de référencement naturel.

## Réalisation

- Durée : **Lundi 19 août 2019 9h00 - lundi 19 août 2019 17h00**. Tout retard ne sera pas corrigé.
- Groupe : **En solo**

## Contraintes

- Frameworks et librairies acceptés.
- Pas de code récupéré.
- Respecter les standards.
- Prendre en compte un maximum de critères OPQUAST.

## Rendu

- Le code source de l'application.

## Spécifications fonctionnelles

D'après le [readme.md de la compétence 1](../e01/readme.md) et les maquettes, intégrer les pages 'Ajouter une blague' étape 1, 'Ajouter une blague' étape 2 et 'Voir une blague'.  
Vous pouvez aussi accéder aux maquettes sur [Figma](https://www.figma.com/file/UwrYmvY5VJJod5a2c1K2dD/Chuck-Norris?node-id=18%3A0).  
Attention, l'integration doit être non fonctionnelle : le javascript et le code côté serveur n'est pas dans le périmètre.  
L'intégration devra tout de même prendre en compte les contraintes de la totalité de l'applications (ne pas oublier les liens par exemple) et devra être visible sur toutes les résolutions.

## Spécifications graphiques

### Typographie

- Texte de base : Roboto light, 16px
- Petit texte : Roboto light, 14px
- Titre : Robot light, 24px

### Couleurs

- Noir : #404040
- Blanc : #FFF
- Gris foncé : #828282
- Gris clair : #F2F2F2
- Bleu : #2D9CDB
- Vert : #219653
- Rouge : #EB5757

## Critères de validation

### Compétence 2 : Réaliser une interface utilisateur web statique et adaptable

- Le contenu de la maquette est rédigé, en français ou en anglais, de façon adaptée à l’interlocuteur et sans faute
- L'interface est conforme à la maquette de l'application
- Les pages web respectent la charte graphique de l'entreprise
- Les bonnes pratiques de structuration et de sécurité sont respectées y compris pour le web mobile
- Les pages web sont accessibles depuis les navigateurs ciblés y compris depuis un mobile
- Les pages web s’adaptent à la taille de l’écran
- Les pages web sont optimisées pour le web mobile
- Le site respecte les règles de référencement naturel
- Les pages web sont publiées sur un serveur
- L’objet de la recherche est exprimé de manière précise en langue française ou anglaise
- La démarche de recherche permet de résoudre un problème technique ou de mettre en oeuvre une nouvelle fonctionnalité
- Le partage du résultat de recherche et de veille est effectué, oralement ou par écrit, avec ses pairs
- Les tests garantissent que les pages web répondent aux exigences décrites dans le cahier des charges [ou dans le dossier technique]
- Les tests de sécurité suivent un plan reconnu par la profession