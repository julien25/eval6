CREATE SCHEMA jokes;

GRANT ALL ON jokes.* TO 'joker'@'localhost' IDENTIFIED BY 'badjoke';

USE jokes;

CREATE TABLE IF NOT EXISTS categories (
    id INTEGER UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL UNIQUE,
    INDEX ix_categories_name(name)
) Engine InnoDB charset UTF8MB4;

CREATE TABLE IF NOT EXISTS sub_categories (
    id INTEGER UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL UNIQUE,
    category_id INTEGER UNSIGNED NOT NULL,
    INDEX ix_sub_categories_category_id (category_id),
    CONSTRAINT FOREIGN KEY fk_sub_categories_category_id (category_id) REFERENCES categories(id)
) Engine InnoDB charset UTF8MB4;

CREATE TABLE IF NOT EXISTS users (
    id INTEGER UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    avatar VARCHAR(250)
) Engine InnoDB charset UTF8MB4;

CREATE TABLE IF NOT EXISTS jokes (
    id VARCHAR(25) PRIMARY KEY,
    `date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    content TEXT,
    sub_category_id INTEGER UNSIGNED NULL,
    INDEX ix_jokes_subcategory_id (sub_category_id),
    CONSTRAINT FOREIGN KEY fk_jokes_sub_category_id (sub_category_id) REFERENCES sub_categories(id)
) Engine InnoDB charset UTF8MB4;

CREATE TABLE IF NOT EXISTS votes (
    user_id INTEGER UNSIGNED NOT NULL,
    joke_id VARCHAR(25) NOT NULL,
    pro BOOL DEFAULT TRUE NOT NULL,
    INDEX votes_user_id (user_id),
    INDEX votes_joke_id (joke_id),
    CONSTRAINT PRIMARY KEY (user_id, joke_id),
    CONSTRAINT FOREIGN KEY votes_user_id(user_id) REFERENCES users(id),
    CONSTRAINT FOREIGN KEY votes_joke_id(joke_id) REFERENCES jokes(id)
) Engine InnoDB charset UTF8MB4;
