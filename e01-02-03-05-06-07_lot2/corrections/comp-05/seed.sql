/* Clear all tables before seed */
DELETE FROM sub_categories;
DELETE FROM categories;
DELETE FROM users;

INSERT INTO categories (name)
VALUES ('Life'), ('Human'), ('Hobbies'), ('Other');

INSERT INTO sub_categories (name, category_id)
SELECT 'Animal', id FROM categories WHERE name = 'Life';

INSERT INTO sub_categories (name, category_id)
SELECT 'Food', id FROM categories WHERE name = 'Life';

INSERT INTO sub_categories (name, category_id)
SELECT 'Carreer', id FROM categories WHERE name = 'Human';

INSERT INTO sub_categories (name, category_id)
SELECT 'Celebrity', id FROM categories WHERE name = 'Human';

INSERT INTO sub_categories (name, category_id)
SELECT 'Dev', id FROM categories WHERE name = 'Human';

INSERT INTO sub_categories (name, category_id)
SELECT 'History', id FROM categories WHERE name = 'Human';

INSERT INTO sub_categories (name, category_id)
SELECT 'Political', id FROM categories WHERE name = 'Human';

INSERT INTO sub_categories (name, category_id)
SELECT 'Religion', id FROM categories WHERE name = 'Human';

INSERT INTO sub_categories (name, category_id)
SELECT 'Science', id FROM categories WHERE name = 'Human';

INSERT INTO sub_categories (name, category_id)
SELECT 'Fashion', id FROM categories WHERE name = 'Hobbies';

INSERT INTO sub_categories (name, category_id)
SELECT 'Movie', id FROM categories WHERE name = 'Hobbies';

INSERT INTO sub_categories (name, category_id)
SELECT 'Music', id FROM categories WHERE name = 'Hobbies';

INSERT INTO sub_categories (name, category_id)
SELECT 'Sport', id FROM categories WHERE name = 'Hobbies';

INSERT INTO sub_categories (name, category_id)
SELECT 'Explicit', id FROM categories WHERE name = 'Hobbies';

INSERT INTO sub_categories (name, category_id)
SELECT 'Money', id FROM categories WHERE name = 'Hobbies';

INSERT INTO sub_categories (name, category_id)
SELECT 'Travel', id FROM categories WHERE name = 'Hobbies';

INSERT INTO users (first_name, last_name)
VALUES
('Aymeric', 'Artero'),
('Ardhoiyn', 'Assani'),
('Laura', 'Bello'),
('Venise', 'Berté'),
('Jérôme', 'Bioulac'),
('Valentin', 'Cloarec'),
('Romain', 'Delanoë'),
('Yoann', 'Joly'),
('Loïc', 'Rey'),
('Cécile', 'Rocquet'),
('Frédéric', 'Rousselin'),
('Moustapha', 'Sarr'),
('Frédérique', 'Tarroux');

INSERT INTO jokes(id, date, content, sub_category_id) VALUES 
('fMf4pLnLSq68XjCjAAaLkA','2016-05-01 10:51:41.584544','Chuck Norris has eaten over 3200 species of animals, 2436 of them insects.',null),
('wNRgdAwpTjuHJg1SgllP-Q','2016-05-01 10:51:41.584544','The sun really shines on Chuck Norris. We just happen to live close enough to him.',null),
('SQFCZhV8RN-k8q80X4IU2w','2016-05-01 10:51:41.584544','Much like a top fuel dragracer, Chuck Norris'' penis is equipped with a parachute.',(SELECT id FROM sub_categories WHERE name='explicit')),
('QuqW5iRjTCisXJvu8F4vmA','2016-05-01 10:51:41.584544','Chuck Norris'' penis can shoot like a AR-15. well damn i wish i had 1 of those penises.',(SELECT id FROM sub_categories WHERE name='explicit')),
('s9X3i_ToT6arfdd0tbT8fQ','2016-05-01 10:51:41.584544','Chuck norris doesnt call the wrong number you answer the wrong phone',null),
('nuDoPa3hTtSw5yn0njEVzg','2016-05-01 10:51:41.584544','Chuck Norris does not use spell check. If he happens to misspell a word, Oxford will simply change the actual spelling of it.',null),
('Y3XtQY1nQAWbZK9LCffH0A','2016-05-01 10:51:41.584544','Many rednecks and rual farmers enjoy Mountain Oysters as a special delicacy within their traditional menus. Chuck Norris, however, prefers to personally harvest and diet upon Mountain Gorilla Oysters.',null),
('NKn0a_3XTeejzdf-NyGKKg','2016-05-01 10:51:41.584544','When Chuck Norris gets stabbed with a knife  the knife starts to bleed',null),
('8fwUfU_iQ2Sjs04I8CaPwg','2016-05-01 10:51:41.584544','Chuck Norris can eviscerate a moose with his toenails.',null),
('7OaYUUp3TmORY4ZYR-6kpQ','2016-05-01 10:51:41.584544','Chuck Norris recently wrote a 2000-page compendium about how to properly clean your belly-button lint. The book won every literary prize around the world in both fiction and non-fiction categories. Even the Kindle Edition of the book has recently surpassed the Bible as the most read book ever.',null),
('hUmpYwXJT5S7cy8uf6gPLg','2016-05-01 10:51:41.584544','Chuck Norris can burn a hole thru 2 steel plates with a single blink of an eye... with a constant stare he can patch the hole back up.',null),
('plIUydgtTWCFfe8Kkf8rog','2016-05-01 10:51:41.584544','Chuck Norris''s belly button does''nt collect lint, it collects steel wool.',null),
('cRcjbqJpQ-GMzIawRcvVlA','2016-05-01 10:51:41.584544','The only time Chuck Norris felt sadness was when he read Nuck Chorris'' jokes.',null),
('0Z88mqK2ToejnUpAUP3gcg','2016-05-01 10:51:41.584544','When Chuck Norris tries to kill himself, he always dodges the killing blow ''cause he''s that awesome.',null),
('PGwTW0kmRIGKMGJCGnXA7A','2016-05-01 10:51:41.584544','It wasn''t the a-bomb that annihilated nagasaki and hiroshima during WWII..it was Chuck Norris jumping out of the plane and punching the ground.',null),
('wxXcAtBySsS4q_GYmUHjPQ','2016-05-01 10:51:41.584544','Chuck Norris. Thats all im gonna say.',null),
('MvNVPZqkQa6KDHjplDgkeg','2016-05-01 10:51:41.584544','Chuck Norris can us an abucus to solve differential calculus equations.',null),
('eIkRF0RGRuG5ioEFVzTdiw','2016-05-01 10:51:41.584544','A comet will erase the human race, the sciencists called the comet Chuck Norris.',null),
('nDEhJiqvS9uiNL0sqZdHvA','2016-05-01 10:51:41.584544','Chuck Norris can shine his boots to perfection just by using his foot and your ass.',null),
('uIj7sBYJRxOrv6eZ9rHayw','2016-05-01 10:51:41.584544','I Chuck Norris, you Chuck Norris, he she me Chuck Norris. Chuck Norris, Chuck Norrising. Chuckology the study of Chuck Norris, it''s first grade Spongebob',null),
('Yaj5umVeTsecHZI3KuRJ_A','2016-05-01 10:51:41.584544','Chuck Norris doesn''t have a nervous system. He has a supremely tough and confident system.',null),
('cn2m6uUxTr2jJ4FG3Yu0rw','2016-05-01 10:51:41.584544','Chuck Norris doesnt drink coffee, coffee drinks Chuck Norris, because everyone knows coffee needs to wake the hell up',null),
('wXABVtkJT_GnW56ahX1qMA','2016-05-01 10:51:41.584544','A strange-but-true phenomenon - whenever there is a natural disaster in the world, Chuck Norris'' stocks soar.',null),
('8mpsOEa0Tger8_A_vzau7w','2016-05-01 10:51:41.584544','Chuck Norris murdered Elvis Presley after he refused to play a concert in Norris'' living room.',null),
('c5qh8flprdg-amdl-rgwtw','2016-05-01 10:51:41.584544','The easiest way to determine Chuck Norris'' age is to cut him in half and count the rings.',(SELECT id FROM sub_categories WHERE name='science')),
('pox2qE13QKuWnx4uo_5ylQ','2016-05-01 10:51:41.584544','Chuck Norris tells black jokes without looking over his shoulder',null),
('SPv79G_bQWuEBMXyCMHynA','2016-05-01 10:51:41.584544','Chuck Norris is the only person on earth who can rhyme ''orange'' with ''lozenge''.',null),
('GkRN2yYDSQS1AZ4QuFusVw','2016-05-01 10:51:41.584544','Chuck Norris gets 95% in a 50-50 partnership.',null),
('ZVABPR9uS2C2OoFn4baGig','2016-05-01 10:51:41.584544','If Chuck Norris gives you a bag of wet mice for a Christmas gift, consider yourself lucky! It could have been something related to his foot and your face.',null),
('svgItx9ZSVa0PJbT7YSSmQ','2016-05-01 10:51:41.584544','If rocky had the eye of the tiger the tiger had the eye of Chuck Norris... Unfortunately chuck Norris rip the tigers eye due to his unworthyness..',null),
('x62RlLcTRP2DXTgJk3ol9w','2016-05-01 10:51:41.584544','Chuck Norris shot the sheriff, killed Kenny, and killed Mr. Boddy in the hall with a roundhouse kick.',null),
('HzOC1SeNQSObaDNnH9yMjw','2016-05-01 10:51:41.584544','Question: Why did the chicken try cross the road? Answer: To get away from Chuck Norris, and it never made it to the other side.',null),
('Gad970JOT6aZ9RDyV5Zlgg','2016-05-01 10:51:41.584544','Chuck Norris takes Viagra to LOSE an erection.',null),
('9-OKjYpcSpGNORREBfEtlA','2016-05-01 10:51:41.584544','Whatever you''re thinking of, Chuck Norris already thought of that.',null),
('IqeVezb6TcGUXFGDDoUVeg','2016-05-01 10:51:41.584544','Chuck Norris doesn''t get drunk. He simply lowers his IQ to yours.',null),
('Ye_jfZytQ7aWbVN2kM3Gtg','2016-05-01 10:51:41.584544','Chuck Norris can cut someone in half.....by just looking at you.',null),
('qbfnqa0pstsl00i2he3g7w','2016-05-01 10:51:41.584544','Chuck Norris once went skydiving, but promised never to do it again. One Grand Canyon is enough.',(SELECT id FROM sub_categories WHERE name='sport')),
('RX3mFqY8S0Wis-Ksgui0KQ','2016-05-01 10:51:41.584544','A big truck driver challenged Chuck Norris to a fight. Chuck asked him "paper or plastic"? The guy asked "what the hell does that mean"? Chuck said "thats the type of bag you''re gonna carry your teeth home in".',null),
('hPciBYzERMmBLs-JOHnbdw','2016-05-01 10:51:41.584544','Chuck Norris can speak hundreds of languages, but the four he mainly uses are English, Profanity, Sarcasm and Real Shit.',null),
('EOREHmOySoGR2k8J9iBvEQ','2016-05-01 10:51:41.584544','Tornados jump into a root cellar when they see Chuck Norris approaching.',null);
