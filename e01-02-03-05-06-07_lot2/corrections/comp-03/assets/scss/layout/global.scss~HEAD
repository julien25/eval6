/***************/
/*    GLOBAL   */
/***************/
%flex {
  display: flex;
}
%iflex {
  display: inline-flex;
}
%grid {
  display: grid;
}
%igrid {
  display: inline-grid;
}
%hidden {
  display: block;
  margin: 0;
  height: 0;
  overflow: hidden;
  text-indent: -9999px;
}
%links {
  color: $c-sub;
  text-decoration: underline;
  transition: $t;

  &:hover {
    color: $c-main;
  }
}
%full {
  box-sizing: border-box;
  padding-left: $s;
  padding-right: $s;
}
%inside-full,
%inside-full-content {
  box-sizing: border-box;
  margin-right: -$s;
  margin-left: -$s;
  width: 100vw;
}

/* global */
body {
  @extend %grid;
  grid-template: 'h'
                 'm' 1fr
                 'f';
  min-height: 100vh;
}
.main {
  grid-area: m;
  max-width: 100vw;
}
.title {
  margin-top: $s*2;
  margin-bottom: $s;
  font-weight: 300;
  font-size: 2rem;

  &::before {
    display: inline-block;
    vertical-align: middle;
    margin-right: 5px;
  }
}
.title-joke {
  @include iconfont(star);
}
.title-jury {
  @include iconfont(cowboy);
}

@media (min-width: $s-lg) {
  %full,
  %inside-full {
    padding-left: calc(50vw - #{$s-half-lg});
    padding-right: calc(50vw - #{$s-half-lg});
  }
  %inside-full,
  %inside-full-content {
    margin-left: calc(-50vw + #{$s-half-lg});
    margin-right: calc(-50vw + #{$s-half-lg});
  }
}
