(()=>{
  // Step 0.1 : get elements
  const joke = document.querySelector('#joke')
  const result = document.querySelector('#result')
  const votes = document.querySelector('#votes')
  let error = ''

  // Step 1 : get the joke
  document.querySelector('#add-joke-btn').addEventListener('click', btn => {
    // CHange content showing
    document.getElementById('add-joke').style.display = "none"
    document.getElementById('joke-answer').style.display = "block"

    // Get joke in API (AJAX)
    fetch('https://api.chucknorris.io/jokes/random').then(response => {
      if (response.ok) {
        response.json().then(res => {
          // Level 2 : Show category
          if (res.categories.length) {
            joke.querySelector('.category span').innerHTML = res.categories[0]
          } else {
            joke.removeChild(joke.querySelector('.category'))
          }

          // Insert joke
          joke.querySelector('.joke-content').innerHTML = res.value

          // Date - change format
          let d = res.created_at.split(' ')[0].split('-')
          joke.querySelector('.joke-date span').innerHTML = `${d[1]}/${d[2]}/${d[0]}`
        })
      } else {
        error = `Le serveur de l'API Chuck Norris ne retourne pas une réponse correcte.`
      }
    }).catch(e => {
      error = `Nous avons détécté le problème suivant : ${e}.` 
    })
  })

  // Step 2 : Get votes
  if(error) {
    alert(error)
  } else {
    votes.addEventListener('submit', e => {
      let votesDone = votes.querySelectorAll('[type=radio]:checked')
      let positiveVote = 0 
      
      // Count all positive votes
      votesDone.forEach(v => {
        if (v.value == 'true') { positiveVote++ }
      })

      // get the positive votes percen
      let poll = parseInt((positiveVote*100)/votesDone.length || 0)

      // Level 1 : no ajax
      // result.querySelector('.inprogress').innerHTML = `Cette blague a été aimée par ${poll}% des votants`

      // Level 2 : ajax
      let force = (poll > 50) ? 'yes' : ((poll < 50) ? 'no' : 'maybe')
      fetch('https://yesno.wtf/api?force=' + force).then(response => {
        if (response.ok) {
          response.json().then(res => {
            // Hide in progress text
            if (result.querySelector('.inprogress')) { result.removeChild(result.querySelector('.inprogress')) }

            // Show background image
            result.style.backgroundImage = `url(${res.image})`

            // Show progress bar
            let progressbar = result.querySelector('.vote')

            progressbar.querySelector('.vote-like').style.width = `${poll}%`
            progressbar.style.display = 'block'
          })
        } else {
          error = `Le serveur de l'API Yes/No ne retourne pas une réponse correcte.`
        }
      }).catch(e => {
        error = `Nous avons détécté le problème suivant : ${e}.` 
      })
      e.preventDefault()
    })
  }
})()