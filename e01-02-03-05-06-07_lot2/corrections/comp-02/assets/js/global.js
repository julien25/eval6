(()=>{
  // Step 0.1 : get elements
  const btn = document.querySelector('#add-joke-btn')
  const joke = document.querySelector('#joke')
  const result = document.querySelector('#result')
  const votes = document.querySelector('#votes')

  // Step 0.1 : redefine flex (with errors)
  let error = ''
  const ajax = (url, serverName, callback) => {
    fetch(url).then(response => {
      if (response.ok) {
        response.json().then(res => callback(res))
      } else {
        error = `Le serveur de ${serverName} ne retourne pas une réponse correcte.`
      }
    }).catch(e => {
      error = `Nous avons détécté le problème suivant : ${e}.` 
    })
  }

  // Step 1 : get the joke
  let getJoke = () => {
    // go to chuck API 
    ajax('https://api.chucknorris.io/jokes/random', "l'API Chuck Norris", r => {
      ajax('/e01-02-03-05-06-07_lot2/_entrants/templates/ajax/jokes.json?id='+r.id, "l'application", rApp => {
        if(rApp.joke) { // the joke already exist we need to load another one
          getJoke()
        } else {
          // Insert joke in node
          if (r.categories.length) {
            joke.querySelector('.category span').innerHTML = r.categories[0]
          } else {
            joke.removeChild(joke.querySelector('.category'))
          }
          joke.querySelector('.joke-content').innerHTML = r.value

          // Date - change format
          let d = r.created_at.split(' ')[0].split('-')
          joke.querySelector('.joke-date span').innerHTML = `${d[1]}/${d[2]}/${d[0]}`

          // Show/hide elements
          document.getElementById('add-joke').setAttribute('aria-hidden', 'true')
          joke.setAttribute('aria-hidden', 'false')
          result.setAttribute('aria-hidden', 'false')
          votes.setAttribute('aria-hidden', 'false')
        }
      })
    })
  }

  // bind button chuck it
  btn.addEventListener('click', getJoke)

  // Check vote
  // Get number of jury
  const jury = votes.querySelectorAll('[type=radio]')
  const nbJury = jury.length/2
  
  jury.forEach(j => {
    j.addEventListener('change', j => {
      let votesDone = votes.querySelectorAll('[type=radio]:checked')
      
      if (!error && (nbJury == votesDone.length)) {
        // Get all datas
        let query = ''

        votesDone.forEach((v,i)=> {
          query += (i ? '&' : '?') + `${v.name}=${v.value}`
        })

        ajax('/e01-02-03-05-06-07_lot2/_entrants/templates/ajax/result.json'+query, "l'application", rApp => {
          let poll = rApp.result
          let force = (poll > 50) ? 'yes' : ((poll < 50) ? 'no' : 'maybe')

          ajax('https://yesno.wtf/api?force=' + force, "l'API Yes No", rApi => {
            // Hide in progress text
            result.removeChild(result.querySelector('.inprogress'))

            // Show background image
            result.style.backgroundImage = `url(${rApi.image})`

            // Show progress bar
            let progressbar = result.querySelector('.vote')
            let pollText = ''

            switch(true) {
              case (poll == 0):
                pollText = 'No comment'
                break
              case (poll < 25):
                pollText = 'Pas terrible'
                break
              case (poll < 50):
                pollText = 'Mouais'
                break
              case (poll == 50):
                pollText = 'Ça passe'
                break
              case (poll < 75):
                pollText = 'Pas mal'
                break
              case (poll < 100):
                pollText = 'Enorme'
                break
              default:
                pollText = 'Topissime'
            }

            progressbar.querySelector('.vote-like').style.width = `${poll}%`
            progressbar.querySelector('span').innerHTML = pollText
            progressbar.setAttribute('aria-label', `Cette blague est approuvée par ${poll}% du jury`)
            progressbar.setAttribute('aria-hidden','false')
          })
        })
      }
    })
  })
})()